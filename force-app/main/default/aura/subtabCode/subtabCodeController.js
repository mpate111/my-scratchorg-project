({
    openTabWithSubtab : function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.openTab({
            url: 'https://www.google.com',
            focus: true
        }).then(function(response) {
            workspaceAPI.openSubtab({
                parentTabId: response,
                url: 'https://www.google.com',
                focus: true
            });
        })
        .catch(function(error) {
            console.log(error);
        });
    }
})