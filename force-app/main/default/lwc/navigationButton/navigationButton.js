import { api, LightningElement, track } from 'lwc';

export default class NavigationButton extends LightningElement {

    @track language = 'En';

    @api hideNextButton(){
        console.log('Inside the navigation button component hideNextButton');
        this.template.queryselectorAll('lightning-button').forEach((element) => {
            if(element.dataset.id == 'nextButton') {
                element.classList.add('slds-hide');
            }
        });
    }

    @api hidePreviousButton(){
        console.log('Inside the navigation button component hidePreviousButton');
        this.template.queryselectorAll('lightning-button').forEach((element) => {
            if(element.dataset.id == 'previousButton') {
                element.classList.add('slds-hide');
            }
        });
    }

    nextClickHandler(event) {
        console.log('Inside the navigation button component nextclickHandler');
        this.dispatchEvent(new CustomEvent('nextClicked', { bubbles : true, composed : true}));
    }

    previousClickHandler(event) {
        console.log('Inside the navigation button component previousClickHandler');
        this.dispatchEvent(new CustomEvent('previousClicked', { bubbles : true, composed : true}));
    }

}